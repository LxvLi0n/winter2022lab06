import java.util.Random;
public class Die{
	
	private int pips;
	private Random r;
	
	//Constructor
	public Die(){
		
		this.pips = 1;
		this.r = new Random();
		
	}
	
	//Get Methods
	public int getPips(){
		
		return this.pips;
		
	}
	
	public Random getRandom(){
		
		return this.r;
		
	}
	
	public void roll(){
		
		this.pips = r.nextInt(6)+1;
		
	}
	
	public String toString(){
		
		return "You just rolled a " +this.pips;
		
	}
	
}
public class ShutTheBox{
	public static void main(String[] args){
		
		System.out.println("Welcome to the game!");
		
		Board board1 = new Board();
		
		boolean gameOver = false;
		
		while(gameOver == false){
			
			System.out.println("Player 1 turn");
			System.out.println(board1);
			
			if(board1.playATurn()){
				
				System.out.println("Player 2 is the Winner!");
				gameOver=true;
				continue;
				
			}else{
				
				System.out.println("PLayer 2 turn");
				System.out.println(board1);
				
			}
			
			if(board1.playATurn()){
				
				System.out.println("Player 1 is the Winner!");
				gameOver=true;
				continue;
				
			}
		}
	}
}
public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	//Constructor
	public Board(){
		
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
		
	}
	
	public String toString(){
		
		String tiles = "";
		
		for(int i = 0; i<this.closedTiles.length; i++){
			
			if(this.closedTiles[i] == false){
				
			tiles += i+1+" ";
			
			}else{
			
				tiles += "X ";
			
			}
		}
		
		return "Board representation: "+ tiles;
	}
	
	//PlayATurn method
	public boolean playATurn(){
		
		this.die1.roll();
		this.die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		int sum = die1.getPips() + die2.getPips();
		
		if(this.closedTiles[sum-1] == false){
			
			this.closedTiles[sum-1] = true;
			System.out.println("Closing tile: "+ sum);
			return false;
			
		}else{
			
		System.out.println("The tile "+ sum +" is already shut");
		return true;
		}
	}
}